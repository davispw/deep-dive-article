# A Deep Dive into Your Stack

Peter Davis
<br><small><time datetime="2021-W20">May, 2021</time></small>

Have you ever met a wizard?

By "wizard", I mean the person who always gets called at 3am, glances at a database response-time graph and knows instantly how to fix it…the one who seemingly knows the answer to every question…who holds, in their head, a massive and detailed working mental model of the system.

If you have, maybe you've wondered how they became that way—is it the 20+ years of experience, or something else?  If you haven't, maybe you're building systems and feeling frustrated that nobody seems to understand the whole picture; or maybe the wizard retired and your team is struggling.  When the website is down and the database is burning, how is it they looked at that graph and know where to start, let alone knowing what it meant and how to fix it?

Starting from the meticulous details you might find inside a wizard's head, let's explore why we should care about it in the first place, a method for building *working mental models*, and applying this to fire-fighting, team training and personal growth, followed by some thoughts on system design and job responsibilities.

# What happens when…
What happens when you type ~~"google.com"~~ "*yourapplication*.com" into your browser?

Like, *everything* that happens?

Down to the gory details.  From the keyboard's [USB HID buffer](https://electronics.stackexchange.com/questions/519953/contents-of-a-keyboard-buffer), to the DNS lookup, the network routing protocols, load balancing, TLS handshake and certificate validation, HTTP headers and response codes, Kerberos authentication, TCP ACKs/NAKs, SQL queries, HTML rendering, JavaScript, memory-mapped file buffers, Java G1 Garbage Collector algorithms, Wi-Fi [carrier sense multiple access with collision avoidance](https://blogs.arubanetworks.com/solutions/wi-fi-6-should-you-believe-the-hype/) (CSMA-CA), memory-mapped file paging, log aggregation, HTTP client connection pools, virtual machine hypervisors, Kubernetes scheduling algorithms, [Paxos vs. RAFT](https://arxiv.org/pdf/2004.05074.pdf) distributed consensus algorithms, Kafka Consumer Group coordination…

Wait, seriously?  Why should you ever care about Wi-Fi CSMA-CA?  In what world should "G1 Garbage Collector" and "VM Hypervisor" be uttered in the same sentence?  These words looked like English at first, but what do they even mean?  This is not in my job description!

(Inspiration: [`github.com/alex`](https://github.com/alex/what-happens-when#readme)'s GitHub repo, and the apocryphal Google interview question.)

In this article, I'm not interested in a particular answer, but why anyone should care.

## Why Do *I* Care?
Introspectively, this question took me down an emotional road, before I settled on two answers: a rational one, and a personal one.
1. Because I'm curious
2. Because I'm a nerd like that
3. Doesn't everybody care?  I mean, we use computers all day long…
4. Why don't *you* care?
5. *(At 3am when the website is down, database is burning and nobody has figured out what is wrong)* **WhY DoN'T yOu EvEN cArE??!?**
6. Why *should* you care?

A: Because when the website is down, the database is burning, and nobody has figured out what is wrong…*somebody* needs to have an accurate-enough **mental model** of the layers from the user to the server to the database's SSD drive and everything in between, so that they can identify where the problem could be.  And know enough of how to **diagnose** all these layers to scientifically confirm or refute theories, narrow down the problem, and figure out how to fix it.  Because monitors, alerts, tools, and documentation are likely less than perfect, and may lie or mislead.  Not that someone has to be an expert in everything.  Just enough to ~~be dangerous~~ ask questions.  To build that working mental model.

(And also, because I'm a nerd like that.)

Are you the wizard?  Am I a wizard?  (How can I write this article pretending to be a wizard?)  **How can I become a wizard?**

## What happens if nobody cares?
* User: It's broken.
* Tier 1 support: Did you try ~~unplugging it for 60 seconds~~ clearing your browser cookies?
* Tier 2 support: Restart the server.
* Dev support: It must be a database problem.
* DBA: I don't see any long-running queries.
* Unix admin: It must be an application problem.
* Networking guy: It must be an application problem.  (Stop calling me!)
* Dev support: *\*bleary eyes\**
* …

When nobody has a complete picture, then maybe the last resort is to assemble a "war room" of experts.  Have fun at the post-mortem!

# Building Working Mental Models: A Team Exercise
When I think of a wizard, I think of the breadth and depth of their mental model, by which they can accurately generate and test new hypotheses under fire.

A working mental model of a system of software and hardware has two parts: a mental picture (or diagram, if you draw it out), and a collection of *scientific hypotheses*.  The hypotheses are of the form "If X were to break, then I expect to observe Y (using tools T and V), and then if I do A, B, and C, then it should fix X and I should observe Z."  When the model meets the real world during a fire, these hypotheses are tested: "I don't observe Y.  Therefore…what can I conclude?"

I can conclude X might not be broken…but it could also be that my model is wrong, or my observation (monitoring system) is misleading.  Or, "I observed Y and did A, B, and C, but it didn't help."  Later, once the real problem is known, I can update my model from the ground truth—that is, I can learn.

## A Daunting Task?
That's a lot of variables—for just one hypothesis.  Software systems can be hugely complex, all considered.  It takes a huge investment of time and training, not to mention arcane knowledge of different technologies gathered over years of tinkering, to get to true wizard level.  It may seem daunting, or even not worthwhile.

So when I ask, "Why should you care?", I imagine that my answer (rationally "because somebody has to" or personally "because I'm a nerd like that") could be very different from yours.  "Because it could help my career?"  "Because my manager hires for personality over experience, encourages personal growth, and wants T-shaped team members?"  "Because DevOps?"  But maybe you're thinking, "I don't."  "It's not my responsibility."  "I'm not a DBA / sysadmin / network admin / datacenter engineer / SRE / #notmyrole."  "I clock out at 5."

So let me pause here to say that a whole lot of this can be done as a team, on company time, out of your "system health" or "training" budgets if nothing else.  The output of diagramming, game-planning and fire drill exercises directly benefits your team and all the team members.  The exercises can meet you at just about any skill level or level of preparedness, and you can do them iteratively.

## Step 1: Diagramming
Draw, by hand and from memory, an end-to-end-to-end diagram of a system you're working on, and **start zooming in**.

Do this as a **team exercise**.  In a room with a bunch of whiteboards if you can, or on paper sharing via Zoom, allocating plenty of time.  Share and compare.

Likely, nobody will have a complete diagram.  Some people focus more on areas they're experts in and might completely miss some other part of the front-end, back-end, database, messaging, debugging, monitoring…  Everyone will have drawn mistakes.  Help each other **adjust your mental models**.

If this is your first such exercise, give it at least a couple of hours, and plan time for follow-up.  If you already have great diagrams and documentation, study it, but then try to whiteboard from memory, zooming in to unexplored detail.

## Step 2: Game Plan
Pick a random component on your diagram, and ask, **"How could this break?"**

1. **Diagnose**: What would I observe?  What tools would I use to see?  How would I diagnose?  Logs?  Monitors?  Dashboards?  Alerts?  Tools?  Servers?  Could this be a symptom of a deeper problem?
2. **Impact**: What would the end user impact be?  Severity?  Urgency?
3. **Fix**: How would I fix it?  Do I have the necessary access and passwords?  Would I be able to fix it myself or is outside expertise required?
4. **Communicate**: If I need more help, do I know who to call?  What evidence would I need to convince them it's their problem?  Who is responsible for notifying users or support teams?  Communication channels?  Escalation procedures?  On-call roster?  24/7 support?

(Probably there are numerous ways any given thing can break.  Can you think of more?)

If your team already has great run-books (a.k.a. SOPs) with all of this information, use it to fill in the answers.  If not, this is a chance to start building documentation.

At this point, if the number of "I don't know" answers start to seem discouraging, that's your que to dive in deeper and learn.  Remember, even if a problem is entirely outside of your own responsibility or ability to fix, you can still learn about the other aspects of the game plan: diagnosis, impact, and escalation, if not the actual fix.

> If the server's log partition were to fill up: then the disk space alert would send an email to foo@example.com; should tail recent logs to see if an unusual flood of errors points to a deeper problem; there should be no impact to users but any other errors would not be logged; on-call dev support would SSH to the server and delete old logs according to our SOP document (Follow-up questions: Has the disk space alert ever been tested? All the right people in the email group?  Everyone tested their SSH keys recently?)

## Step 3: Fire Drill
Use the diagram and game plan for your next team **fire drill**.  One person plays the drill-master: pick a random component on the diagram and break it (in pre-prod…maybe).  Others race to find and fix it.  (Prizes?)  Follow up by sharing, updating documentation, building missing logging or monitoring.

The point of the drills is two-fold.  On the one hand, you're getting hands-on training and testing your monitoring, tools, and documentation.

More importantly, you're challenging your mental models.  Looking back to Game Planning, you answered, "How could this break, and what would I observe?"  This is a **scientific hypothesis**.  Flipping it around, "If I *don't* observe what I expected, then what does it imply?"  It could imply that this component is not the culprit, but it could be instead that your model is wrong, or your monitoring or diagnostic tools are broken, too.  The fire drill is training the application of the scientific method, challenging your hypotheses.

After the drill, hold a post-mortem.  If any hypotheses were disproven, you can update your working model from the **ground truth**.  Meaning, in the controlled environment of a fire drill, the drill-master knows what was *actually* broken.

(If you have a real-world post-mortem, you can do the same, retroactively.)

## And Repeat
Zoom in on a different part of the diagram, maybe one where your teammates are least comfortable.  The goal is to build that working mental model—to train your brain.  Learning takes repetition and practice.

In the next part of this article, I'll show an example of putting this into practice and then explain more about the value.

## Some Examples
Not really talking about straightforward debugging.  "Oh, I see NullPointerExceptions in the logs. Patch, deploy, fixed, done."

I'm talking about: Users getting 503 errors, but the message plus stack trace was so large that the ElasticSearch log aggregator barfed and didn't record it…so spent hours chasing wild geese between the load balancer, firewall and servlet engine, ruling out every other possibility…before someone thought to grep the raw logs on the server.  (Duh!)

Obvious in hindsight, but then why was it so hard?

Oh!  We're missing the log aggregation and indexing pipeline on our system diagram!

It'd be easy to forget.  A whole separate system on the side that normally has nothing to do with the application's function.  Probably built by another team in another lifetime.  Normally log search is just another tool we use.

Solving this required both understanding all the other layers—to rule them out by hypothesis and trial—and understanding that this whole layer on the side *exists*…and can break.

## More Examples
* Unix filesystem backup taking a copy-on-write "snapshot" every night at a random time (depending on centralized tape storage scheduling); during the backup, if [Apache Solr](https://solr.apache.org) happened to start a [recovery job](https://solr.apache.org/guide/7_0/solrcloud-recoveries-and-write-tolerance.html), it could spike 2x the normal disk space; when the disk hit 100% full, disk I/O would *hang* hard…but of course, the snapshot is happening at a very low level, and our disk space stats didn't show it.
* 1 of 2 Kubernetes DNS server instances went down but still reported "healthy", causing DNS lookups to randomly fail.  Normally, kube-dns handles cluster-internal DNS and transparently forwards requests to external DNS servers, so it's easy to forget that this layer exists…and can break.
* VMWare VMotion would randomly decide to migrate ZooKeeper VMs to a different physical server (a supposedly transparent and instantaneous process), during which there is a brief pause in disk I/O, causing ZooKeeper sessions to timeout…but VMWare logs and stats were locked down such that only the VM admins can see; app team spent weeks trying to rule out networking issues and garbage collector issues.

(Don't have a clue what these things are?  Don't care?  Read on.)

# How Can I Become a Wizard?
Team exercises can go a long way…but only so far.  I mean, just mapping out all of the layers of a system—and practicing through fire drills (or real fires)—is extremely valuable.  But going deep enough into each piece to be able to understand, investigate and debug any random piece of the puzzle under pressure probably takes more time than company training budget allows for.

Ultimately, if you don't care, you're not likely to put in the [time](https://www.vox.com/science-and-health/2019/8/23/20828597/the-10000-hour-rule-debunked) and practice.  Not rote practice, but the kind of practice you are personally willing to invest yourself in.  Frankly, someone can be a really good programmer, but if learning stops outside of 9-5 and their IDE, it shows.

I believe learning across the breadth of computer science and engineering has to happen at a personal level.

## Materials

* Podcasts while driving
* YouTube rabbit holes
* [https://reddit.com/r/*yourfavoritecommunityhere*](https://reddit.com/)
* ~~[Slashdot](https://slashdot.org)~~ [Hacker News](https://news.ycombinator.com)
* Computer history—[Apollo Guidance Computer](https://history.nasa.gov/computers/Ch2-5.html) ([AGC](https://youtu.be/xx7Lfh5SKUQ)), video game consoles
* [Security flaws](https://meltdownattack.com)
* [Public post-mortems](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/41)

## Practice

* [Hobbies](https://marlinfw.org/docs/basics/introductidon.html)
* [Build a new project](https://nodejs.dev/learn) from scratch
* Use a new stack
* Use a new programming language
* Practice shell scripting and regular expressions
* Install Linux and administer your PC
* Build a [Raspberry Pi Cluster](https://blog.alexellis.io/test-drive-k3s-on-raspberry-pi/)

## But I don't have the time!

* ["Learning Hour"](https://www.futurelearn.com/info/blog/making-futurelearn/running-a-learning-hour) with your team
* Attend a [conference](https://plus.qconferences.com)—or just watch the videos. (Ask your supervisor)
* Training budget
* Master's degree?  Certification courses?  (If it's right for you)
* Consider it an investment in your career

# Designing This Away
Hiring for all these skills is expensive, if not impossible.  Managers talk about hiring for personality over experience, but hiring is hard enough that it's almost moot.  Site Reliability Engineers, DevOps, Full Stack Developers…unicorns?  So if only the system…
* ~~were just simpler~~
* ~~were just better designed~~
* ~~just had better monitoring that could pinpoint the problem~~
* ~~were just tested better~~
* ~~just didn't have so many failure points~~
* ~~just had better logging that could pinpoint the problem~~
* ~~just had a Single Pane of Glass~~
* ~~just had better observability~~
* were perfect…

…or if the team…
* ~~just had the right people~~
* ~~just had enough staff~~
* ~~just had better training~~
* ~~just didn't have knowledge silos~~
* ~~just had access to all the tools we need~~
* ~~just had better runbook documentation~~
* ~~could just rely on the other teams to proactively fix things~~
* were perfect…

…then everyone could just do their jobs according to their specific job descriptions, and we wouldn't need such breadth of expertise, right?

Let me challenge these "if only justs".  (And let me say when you're feeling frustrated or accused, it might help to keep these questions in mind.)

How does one design the simplest possible system, but no simpler?

How does one build *effective* monitoring that pinpoints the problem, not just the symptoms?

How does one test real scenarios, uncover failure points, add missing logging, build a Single Pane of Glass (that actually helps), or know how to interpret observations?  How does one get enough of the "right" people, train better, tear down silos, get access to tools, write better runbooks, or improve escalation with other teams?

I believe that designing the perfect system or having the perfect team, ultimately, requires mapping out and understanding a breadth of interactions across the system—what all can break and how to observe, diagnose, fix and communicate about it.  That is, building *working mental models* and putting them into practice.  Moreover, whether on company time or through personal curiosity and exploration, people must convince themselves that they should *care*.  So, let's start there.
